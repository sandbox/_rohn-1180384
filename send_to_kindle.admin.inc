<?php

/**
 * @file
 * Administration page for the Send to Kindle Button.
 */

/**
 * Menu callback: displays the Send to Kindle module settings page.
 *
 * @ingroup forms
 */
function send_to_kindle_admin_settings() {

  $form['send_to_kindle_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display the Send to Kindle button on these content types:'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('send_to_kindle_node_types', array('story')),
  );
  $form['send_to_kindle_showonteasers'] = array(
    '#type' => 'radios',
    '#title' => t('Display button on teasers?'),
    '#options' => array(t('No'), t('Yes')),
    '#default_value' => variable_get('send_to_kindle_showonteasers', 0),
    '#description' => t('If <em>Yes</em> is selected, the button will appear when the node is displayed in teaser mode. Otherwise it will only appear when the full node is being viewed.'),
  );
  $form['send_to_kindle_weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 50,
    '#default_value' => variable_get('send_to_kindle_weight', '-5'),
    '#description' => t('Heavier items will sink. Default weight -5 will show the button at the top of the node content.'),
  );
  return system_settings_form($form);
}
